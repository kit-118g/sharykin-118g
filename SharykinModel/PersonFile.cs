﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharykinModel
{
    [Serializable]
    public class PersonFile
    {
        public Person Person { get; set; }

        public DateTime EnterTime { get; set; }

        public string Group { get; set; }

        public string Facultet { get; set; }

        public string Specializiation { get; set; }

        public double Progress { get; set; }

        public PersonFile()
        {
            Person = new Person();
        }

        public override string ToString()
        {
            string data = Person.ToString() + $"\nEnter date: {EnterTime:d}\nIndex group: {Group}" +
                          $"\nFacultet: {Facultet}\nSpecializiation: {Specializiation}\nProgress: {Progress}";

            return data;
        }

        public string StringForFile()
        {
            Facultet = Facultet.Replace(' ', '_');
            Specializiation = Specializiation.Replace(' ', '_');
            string data = Person.StringForFile() + $" {EnterTime:d} {Group} {Facultet} " +
                          $"{Specializiation} {Progress}";

            return data;
        }
    }
}