﻿using System;

namespace SharykinModel
{
    [Serializable]
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime Birthday { get; set; }

        public Person()
        {
            FirstName = "Vasya";
            LastName = "Pupkin";
            MiddleName = "Vasilyavich";
            Birthday = new DateTime(2000, 1, 1);
        }

        public override string ToString()
        {
            string personData = $"First name: {FirstName}\nLast name: {LastName}\nMiddle name: " +
                                $"{MiddleName}\nBirthday: {Birthday.ToString("d")}";
            return personData;
        }

        public string StringForFile()
        {
            string data = $"{FirstName} {LastName} {MiddleName} {Birthday:d}";

            return data;
        }
    }
}