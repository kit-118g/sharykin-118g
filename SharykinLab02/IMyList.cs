﻿using System.Collections;

namespace SharykinLab02
{
    public interface IMyList<T> : IEnumerator, IEnumerable
    {
        void AddAll(T[] array);

        void Add(T t);

        void Add(T t, int index);

        T Replace(T t, int index);

        T Remove(T t);

        T Remove(int index);

        int Size();

        bool Contains(T t);

        T Get(int index);

        void Clear();
    }
}