﻿using System;
using SharykinModel;

namespace SharykinLab02
{
    class Program
    {
        static void Main()
        {
            PersonalFileList<PersonFile> files = new PersonalFileList<PersonFile>();

            PersonFile file1 = new PersonFile
            {
                EnterTime = new DateTime(2018, 9, 1),
                Facultet = "CIT",
                Group = "118G",
                Person = new Person
                {
                    Birthday = new DateTime(2000, 9, 28),
                    FirstName = "Konstantin",
                    LastName = "Sharykin",
                    MiddleName = "Valyrievich"
                },
                Progress = 50,
                Specializiation = "CE"
            };
            files.Add(file1);
            foreach (var file in files)
            {
                Console.WriteLine(file.Equals(file1));
            }
        }
    }
}