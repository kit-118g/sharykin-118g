﻿using System;
using System.Collections;
using System.Xml.Serialization;
using SharykinModel;

namespace SharykinLab02
{
    [Serializable]
    [XmlInclude(typeof(PersonFile))]
    public class PersonalFileList<T> : IMyList<T>
    {
        private T[] array;

        private int currentIndex;

        private int iterationPosition;

        private readonly int defaultInitialSize = 10;

        public PersonalFileList()
        {
            currentIndex = 0;
            iterationPosition = 0;
            array = new T[defaultInitialSize];
        }

        public PersonalFileList(int initialSize)
        {
            currentIndex = 0;
            iterationPosition = 0;
            array = new T[initialSize];
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public bool MoveNext()
        {
            return (iterationPosition++ < currentIndex);
        }

        public void Reset()
        {
            iterationPosition = 0;
        }

        public object Current
        {
            get { return array[iterationPosition - 1];}
        }

        private void VerifyIndex(int index)
        {
            if (index >= currentIndex || index < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public int Size()
        {
            return currentIndex;
        }

        public void Add(T t)
        {
            if(currentIndex >= array.Length)
            {
                Array.Resize(ref array, array.Length * 2);
            }
            array[currentIndex] = t;
            currentIndex++;
        }

        public void Add(T t, int index)
        {
            VerifyIndex(index);

            if (currentIndex >= array.Length)
            {
                Array.Resize(ref array, array.Length * 2);
            }

            Array.Copy(array, index, array, index+1, array.Length - (index + 1));
            array[index] = t;
            currentIndex++;
        }

        public void AddAll(T[] inputArray)
        {
            if(array.Length > 0)
            {
                Array.Resize(ref array, array.Length + inputArray.Length);
                Array.Copy(inputArray, 0, array, currentIndex, inputArray.Length);
                currentIndex += inputArray.Length;
            }
        }

        public T Replace(T t, int index)
        {
            VerifyIndex(index);
            T replacedElement = array[index];
            array[index] = t;

            return replacedElement;
        }

        public T Remove(T t)
        {
            T removedElement = default(T);
            for(int i = 0; i < array.Length; i++)
            {
                if(array[i].Equals(t))
                {
                    removedElement = array[i];
                    Array.Copy(array, i + 1, array, i, array.Length - (i + 1));
                    currentIndex--;

                    break;
                }
            }

            return removedElement;
        }

        public T Remove(int index)
        {
            VerifyIndex(index);
            T removedElement = array[index];
            Array.Copy(array, index, array, index + 1, array.Length - (index + 1));
            currentIndex--;
           
            return removedElement;
        }

        public bool Contains(T t)
        {
            foreach(T element in array)
            {
                if(element != null && element.Equals(t))
                {
                    return true;
                }
            }

            return false;
        }

        public T Get(int index)
        {
            VerifyIndex(index);

            return array[index];
        }

        public void Clear()
        {
            array = new T[defaultInitialSize];
            currentIndex = 0;
        }
        
        public void Add(Object obj)
        {
            if (currentIndex >= array.Length)
            {
                Array.Resize(ref array, array.Length * 2);
            }
            array[currentIndex] = (T)obj;
            currentIndex++;
        }
    }

}