﻿using SharykinModel;
using SharykinLab01.Views;

namespace SharykinLab01
{
    public class Program
    {
        public const int SIZE = 1;
        public static PersonFile[] files;
        public static void Main()
        {
            files = new PersonFile[SIZE];

            int counter = 0;

            while(counter < SIZE)
            {
                files[counter] = FileView.FillPersonFile();
                counter++;
            }

            FileView.Show(files);
        }
    }
}