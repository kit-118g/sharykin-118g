﻿using SharykinModel;
using System;

namespace SharykinLab01.Views
{
    public static class FileView
    {
        public static PersonFile FillPersonFile()
        {
            var person = new Person();
            Console.Write("Input first name: ");
            person.FirstName = Console.ReadLine();
            Console.Write("\nInput last name: ");
            person.LastName = Console.ReadLine();
            Console.Write("\nInput middle name: ");
            person.MiddleName = Console.ReadLine();

            int counter = 0;
            int[] date = new int[3];
            string[] nameDates = new string[] { "day", "month", "year" };
            string data = "";

            while (counter < 3)
            {
                Console.Write("\nInput {0}: ", nameDates[counter]);
                data = Console.ReadLine();
                if (!int.TryParse(data, out date[counter]))
                {
                    Console.WriteLine("Wrong data");
                    continue;
                }
                counter++;
            }

            person.Birthday = new DateTime(date[2], date[1], date[0]);

            var newPersonFile = new PersonFile();
            newPersonFile.Person = person;

            Console.Write("\nGroup: ");
            newPersonFile.Group = Console.ReadLine();
            Console.Write("\nInput facultet: ");
            newPersonFile.Facultet = Console.ReadLine();

            Console.Write("\nInput specializiation: ");
            newPersonFile.Specializiation = Console.ReadLine();

            do
            {
                double value;
                Console.Write("\nInput progress (without %): ");
                data = Console.ReadLine();

                if (double.TryParse(data, out value))
                {
                    newPersonFile.Progress = value;
                    break;
                }
                else
                {
                    Console.WriteLine("Wrong data");
                }
            } while (true);

            return newPersonFile;
        }

        public static void Show(PersonFile[] files)
        {
            foreach (var personFile in files)
            {
                Console.WriteLine("--------------------------");
                Console.WriteLine(personFile.ToString());
                Console.WriteLine("--------------------------");
            }
        }
    }
}
