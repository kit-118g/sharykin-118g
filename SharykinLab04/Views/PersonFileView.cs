﻿using System;
using System.Text;
using SharykinLabs04.Controllers;
using SharykinModel;

namespace SharykinLabs04.Views
{
    public static class PersonFileView
    {
        private static PersonFileController _controller = new PersonFileController();
        public static void ShowStudentGroup(PersonFile file)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("This student in group ");
            sb.Append(file.Facultet);
            sb.Append("-");
            sb.Append(file.Group);
            sb.Append(" was entered ");
            sb.Append(file.EnterTime.ToString("d"));
            
            Console.WriteLine(sb);
        }

        public static void ShowStudentYear(PersonFile file)
        {
            var result = _controller.CalculateStudyYear(file.EnterTime);

            Console.WriteLine("This student has {0} semestr. It's {1} year study",
                result[_controller.FIRST_KEY], result[_controller.SECOND_KEY]);
        }

        public static void ShowAge(PersonFile file)
        {
            Console.WriteLine(_controller.ExactBirthday(file.Person.Birthday));
        }
    }
}