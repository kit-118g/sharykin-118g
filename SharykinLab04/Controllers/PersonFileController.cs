﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharykinLabs04.Controllers
{
    public class PersonFileController
    {
        public readonly string FIRST_KEY = "Semestr";
        public readonly string SECOND_KEY = "StudyYear";
        public Dictionary<string, int> CalculateStudyYear(DateTime enteredTime)
        {
            int totalSemestr = 0;
            int totalYear = 0;
            Dictionary<string, int> studyYear = new Dictionary<string, int>();
            DateTime now = DateTime.Now;
            
            int daysDiff = (now - enteredTime).Days;

            while (daysDiff > 0)
            {
                daysDiff -= 182;
                totalSemestr++;
            }

            totalYear = totalSemestr / 2 + totalSemestr % 2;
            
            studyYear.Add(FIRST_KEY, totalSemestr);
            studyYear.Add(SECOND_KEY, totalYear);

            return studyYear;
        }

        public string ExactBirthday(DateTime birthday)
        {
            var diff = DateTime.Today - birthday;
            var result = new DateTime(1, 1, 1).AddDays(diff.Days);
            result.AddYears(-1);
            result.AddDays(-1);
            result.AddMonths(-1);

            return "This student is " + result.Year + " years, "
                   + result.Month + " month " + result.Day + " days";
        }
    }
}