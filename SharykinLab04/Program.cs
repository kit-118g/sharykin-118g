﻿using System;
using System.Collections.Generic;
using SharykinLabs04.Views;
using SharykinModel;

namespace SharykinLabs04
{
    class Program
    {
        static void Main(string[] args)
        {
            List<PersonFile> files = new List<PersonFile>
            {
                new PersonFile()
                {
                    EnterTime = new DateTime(2018, 9, 1),
                    Facultet = "CIT",
                    Group = "118a",
                    Person = new Person()
                    {
                        Birthday = new DateTime(2001, 02, 14),
                        FirstName = "Nina",
                        LastName = "Medved",
                        MiddleName = "Alekseevna"
                    },
                    Progress = 100,
                    Specializiation = "Computer Engineering",
                },
                new PersonFile()
                {
                    EnterTime = new DateTime(2018, 9, 1),
                    Facultet = "CIT",
                    Group = "118g",
                    Person = new Person()
                    {
                        Birthday = new DateTime(2000, 9, 28),
                        FirstName = "Konstantin",
                        LastName = "Sharykin",
                        MiddleName = "Valerievich"
                    },
                    Progress = 53,
                    Specializiation = "Computer Engineering",
                }
            };
            
            PersonFileView.ShowStudentGroup(files[0]);
            PersonFileView.ShowStudentYear(files[1]);
            PersonFileView.ShowAge(files[1]);
        }
    }
}