﻿using System;
using System.Linq;
using SharykinLab02;
using SharykinModel;

namespace SharykinLab06
{
    public class SimpleLINQ
    {
        public void OutputGroups(PersonalFileList<PersonFile> PersonFiles)
        {
            var names =
                from PersonFile s in PersonFiles
                select s.Person.FirstName;

            var groups =
                from PersonFile s in PersonFiles
                select s.Group;

            Console.WriteLine("Groups:");
            foreach (var ng in names.Zip(groups, Tuple.Create))
            {
                Console.WriteLine("|{0,10}|{1,3}|", ng.Item1, ng.Item2);
            }
        }

        public void OutputCourses(PersonalFileList<PersonFile> PersonFiles)
        {
            var now = DateTime.Now;

            var names =
                from PersonFile s in PersonFiles
                select s.Person.FirstName;

            var admissions =
                from PersonFile s in PersonFiles
                select now.Year - s.EnterTime.Year;

            Console.WriteLine("By admissions:");
            foreach (var nc in names.Zip(admissions, Tuple.Create))
            {
                if (now.Month >= 9 && now.Month <= 12)
                {
                    Console.WriteLine("|{0,10}|{1,3}|{2,3}|", nc.Item1, nc.Item2 + 1, 1);
                }
                else
                {
                    Console.WriteLine("|{0,10}|{1,3}|{2,3}|", nc.Item1, nc.Item2, 2);
                }
            }
        }

        public void OutputAge(PersonalFileList<PersonFile> PersonFiles)
        {
            var now = DateTime.Now;

            var names =
                from PersonFile s in PersonFiles
                select s.Person.FirstName;
            var years =
                from PersonFile s in PersonFiles
                select now.Year - s.EnterTime.Year;

            Console.WriteLine("By age:");
            foreach (var ny in names.Zip(years, Tuple.Create))
            {
                Console.WriteLine("|{0,10}|{1,10}|", ny.Item1, ny.Item2);
            }

        }
    }

}