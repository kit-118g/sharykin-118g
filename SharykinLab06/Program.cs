﻿using System;
using SharykinLab02;
using SharykinModel;

namespace SharykinLab06
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonFile[] students = new PersonFile[5] {
                new PersonFile(), new PersonFile(), new PersonFile(), new PersonFile(), new PersonFile()
            };

            PersonalFileList<PersonFile> PersonFileList = new PersonalFileList<PersonFile>();
            SimpleLINQ linq = new SimpleLINQ();

            students[0].Person.FirstName = "PersonFile 1 FN";
            students[0].Person.MiddleName = "PersonFile 1 SN";
            students[0].Person.LastName = "PersonFile 1 LN";
            students[0].Person.Birthday = Convert.ToDateTime("20/10/2000");
            students[0].EnterTime = Convert.ToDateTime("20/10/2012");
            students[0].Facultet = "PersonFile 1 Facultet";
            students[0].Group = "PersonFile 1 Group index";
            students[0].Specializiation = "PersonFile 1 Speciality";
            students[0].Progress = 10;

            students[1].Person.FirstName = "PersonFile 2 FN";
            students[1].Person.MiddleName = "PersonFile 2 SN";
            students[1].Person.LastName = "PersonFile 2 LN";
            students[1].Person.Birthday = Convert.ToDateTime("15/07/2003");
            students[1].EnterTime = Convert.ToDateTime("20/10/2015");
            students[1].Facultet = "PersonFile 2 Facultet";
            students[1].Group = "PersonFile 2 Group index";
            students[1].Specializiation = "PersonFile 2 Speciality";
            students[1].Progress = 7;

            students[2].Person.FirstName = "PersonFile 3 FN";
            students[2].Person.MiddleName = "PersonFile 3 SN";
            students[2].Person.LastName = "PersonFile 3 LN";
            students[2].Person.Birthday = Convert.ToDateTime("01/11/2001");
            students[2].EnterTime = Convert.ToDateTime("20/10/2013");
            students[2].Facultet = "PersonFile 3 Facultet";
            students[2].Group = "PersonFile 3 Group index";
            students[2].Specializiation = "PersonFile 3 Speciality";
            students[2].Progress = 12;

            students[3].Person.FirstName = "PersonFile 4 FN";
            students[3].Person.MiddleName = "PersonFile 4 SN";
            students[3].Person.LastName = "PersonFile 4 LN";
            students[3].Person.Birthday = Convert.ToDateTime("07/02/2002");
            students[3].EnterTime = Convert.ToDateTime("20/10/2014");
            students[3].Facultet = "PersonFile 4 Facultet";
            students[3].Group = "PersonFile 4 Group index";
            students[3].Specializiation = "PersonFile 4 Speciality";
            students[3].Progress = 9;

            students[4].Person.FirstName = "PersonFile 5 FN";
            students[4].Person.MiddleName = "PersonFile 5 SN";
            students[4].Person.LastName = "PersonFile 5 LN";
            students[4].Person.Birthday = Convert.ToDateTime("13/04/1997");
            students[4].EnterTime = Convert.ToDateTime("20/10/2009");
            students[4].Facultet = "PersonFile 5 Facultet";
            students[4].Group = "PersonFile 5 Group index";
            students[4].Specializiation = "PersonFile 5 Speciality";
            students[4].Progress = 6;

            PersonFileList.Add(students[4]);
            PersonFileList.Add(students[3]);
            PersonFileList.Add(students[2]);
            PersonFileList.Add(students[1]);
            PersonFileList.Add(students[0]);

            linq.OutputGroups(PersonFileList);
            Console.WriteLine();
            linq.OutputCourses(PersonFileList);
            Console.WriteLine();
            linq.OutputAge(PersonFileList);
            Console.ReadKey();

        }
    }
}