﻿using System;
using System.IO;
using System.Xml.Serialization;
using SharykinLab02;
using SharykinLab05.Controllers;
using SharykinModel;

namespace SharykinLab05
{
    class Program
    {
        static void Main()
        {
            IMyList<PersonFile> list = new PersonalFileList<PersonFile>()
            {
                new PersonFile()
                {
                    EnterTime = new DateTime(2018, 9, 1),
                    Facultet = "CIT",
                    Group = "118a",
                    Person = new Person()
                    {
                        Birthday = new DateTime(2001, 02, 14),
                        FirstName = "Nina",
                        LastName = "Medved",
                        MiddleName = "Alekseevna"
                    },
                    Progress = 100,
                    Specializiation = "Computer Engineering",
                },
                new PersonFile()
                {
                    EnterTime = new DateTime(2018, 9, 1),
                    Facultet = "CIT",
                    Group = "118g",
                    Person = new Person()
                    {
                        Birthday = new DateTime(2000, 9, 28),
                        FirstName = "Konstantin",
                        LastName = "Sharykin",
                        MiddleName = "Valerievich"
                    },
                    Progress = 53,
                    Specializiation = "Computer Engineering",
                }
            };
            Delegator demo = new Delegator();
            XmlSerializer xml = new XmlSerializer(typeof(PersonalFileList<PersonFile>));

            int size = 0;
            int command = 0;
            int selectedDel = 0;
            string field;

            demo.avarage = demo.avarageAge;
            demo.avarage += demo.avarageProgress;
            
            // //Deserialize
            // using (FileStream fs = new FileStream("PersonFiles.xml", FileMode.OpenOrCreate))
            // {
            //     list = (IMyList<PersonFile>) xml.Deserialize(fs);
            // }


            foreach (PersonFile s in list)
            {
                Console.WriteLine(s);
            }
            
            //Show PersonFile
            Console.WriteLine("Enter field by which you want see result: ");
            field = Console.ReadLine();
            demo.output(list, field);
            
            //Delete
            Console.WriteLine("Enter field by which you want delete: ");
            field = Console.ReadLine();
            demo.delete(list, field);
            
            //Choose delegate
            Console.Write(
                "1. By Group Index\n2. By Faculty\n3. By Specialty\n4. Cancel\nEnter deligate(1-3): ");
            selectedDel = int.Parse(Console.ReadLine());
            switch (selectedDel)
            {
                case 1:
                    demo.compare = demo.CompareByGroup;
                    break;
                case 2:
                    demo.compare = demo.CompareByFaculty;
                    break;
                case 3:
                    demo.compare = demo.CompareBySpecialty;
                    break;
            }
            
            //Average
            Console.Write("Enter field by which you want get average: ");
            field = Console.ReadLine();

            demo.avarage(list, field);
            
            //Serialize
            using (FileStream fs = new FileStream("PersonFiles.xml", FileMode.OpenOrCreate))
            {
                xml.Serialize(fs, list);
            }
        }
    }
}