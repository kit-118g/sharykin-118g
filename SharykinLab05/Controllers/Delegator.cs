﻿using System;
using SharykinModel;
using SharykinLab02; 

namespace SharykinLab05.Controllers
{
    delegate bool CompareBy(PersonFile PersonFile, string field);
    delegate void Avarage(IMyList<PersonFile> PersonFiles, string field);

    class Delegator
    {
        public CompareBy compare { get; set; }
        public Avarage avarage { get; set; }

        public void output(IMyList<PersonFile> PersonFiles, string field)
        {
            if (compare != null)
            {
                string formatRow = "|{0,10}|{1,10}|{2,10}|{3,10}|{4,10}|{5,2}|{6,5}|{7,5}|{8,3}|";

                foreach (PersonFile s in PersonFiles)
                {
                    if (compare(s, field))
                    {
                        Console.WriteLine(formatRow, s);
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose delegate before showing");
            }
        }

        public void delete(IMyList<PersonFile> PersonFiles, string field)
        {
            if (compare != null)
            {
                for (int i = 0; i < PersonFiles.Size(); i++)
                {
                    if (compare(PersonFiles.Get(i), field))       // Get starts from 0
                    {
                        PersonFiles.Remove(i + 1);                 // Remove starts from 1
                        i--;
                    }
                }
            }
            else
            {
                Console.WriteLine("Choose Deligate before deleteing");
            }
        }

        public void avarageAge(IMyList<PersonFile> PersonFiles, string field)
        {
            if (compare != null)
            {
                float avarage = 0;

                foreach (PersonFile s in PersonFiles)
                {
                    if (compare(s, field))
                    {
                        avarage += DateTime.Now.Year - s.Person.Birthday.Year;
                    }
                }

                avarage /= PersonFiles.Size();

                Console.WriteLine("Avarage Age: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public void avarageProgress(IMyList<PersonFile> PersonFiles, string field)
        {
            if (compare != null)
            {
                double avarage = 0;

                foreach (PersonFile s in PersonFiles)
                {
                    if (compare(s, field))
                    {
                        avarage += s.Progress;
                    }
                }

                avarage /= PersonFiles.Size();

                Console.WriteLine("Avarage Progress: " + avarage);
            }
            else
            {
                Console.WriteLine("Choose Deligate");
            }
        }

        public bool CompareByGroup(PersonFile PersonFile, string group)
        {
            return PersonFile.Group.Equals(group);
        }

        public bool CompareByFaculty(PersonFile PersonFile, string faculty)
        {
            return PersonFile.Facultet.Equals(faculty);
        }

        public bool CompareBySpecialty(PersonFile PersonFile, string specialty)
        {
            return PersonFile.Specializiation.Equals(specialty);
        }
    }

}