﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SharykinModel;

namespace SharykinLab03.Controllers
{
    public class PersonFileController
    {
        public List<PersonFile> ReadFile(string path)
        {
            List<PersonFile> personFiles = new List<PersonFile>();

            using(FileStream fstream = File.OpenRead(path))
            {
                byte[] array = new byte[fstream.Length];

                fstream.Read(array, 0, array.Length);

                string textFromFile = Encoding.Default.GetString(array);

                string[] data = textFromFile.Split('\n');
                foreach(var person in data)
                {
                    string[] pieceData = person.Split(' ');
                    PersonFile file = new PersonFile();

                    file.Person.FirstName = pieceData[0];
                    file.Person.LastName = pieceData[1];
                    file.Person.MiddleName = pieceData[2];
                    file.Person.Birthday = DateTime.Parse(pieceData[3]);
                    file.EnterTime = DateTime.Parse(pieceData[4]);
                    file.Group = pieceData[5];
                    file.Facultet = pieceData[6].Replace('_', ' ');
                    file.Specializiation = pieceData[7].Replace('_', ' ');
                    file.Progress = double.Parse(pieceData[8]);

                    personFiles.Add(file);
                }
            }

            return personFiles;
        }

        public void WriteFile(List<PersonFile> personFiles, string path)
        {
            using (FileStream fstream = new FileStream(path, FileMode.Truncate))
            {
                int i = 0;
                foreach(var personFile in personFiles)
                {
                    string result = personFile.StringForFile();
                    if (i != personFiles.Capacity - 2)
                    {
                        result += "\n";
                    }
                    byte[] array = Encoding.Default.GetBytes(result);

                    fstream.Write(array, 0, array.Length);
                    i++;
                }
            }
        }
    }
}