﻿using System;
using System.Collections.Generic;
using SharykinLab03.Controllers;
using SharykinModel;

namespace SharykinLab03
{
    class Program
    {
        private const string PATH = "../../../Data.txt";
        static void Main()
        {
            PersonFileController _controller = new PersonFileController();
            List<PersonFile> personFiles = _controller.ReadFile(PATH);

            personFiles[2].EnterTime = DateTime.Parse("01.09.2019");
            personFiles[2].Group = "119G";

            foreach(PersonFile person in personFiles)
            {
                Console.WriteLine(person.ToString());
            }
            Console.WriteLine("-----------------------");
            personFiles = _controller.ReadFile(PATH);
            foreach (PersonFile person in personFiles)
            {
                Console.WriteLine(person.ToString());
            }
            Console.WriteLine("-----------------------");
            personFiles[2].EnterTime = DateTime.Parse("01.09.2019");
            personFiles[2].Group = "119G";

            _controller.WriteFile(personFiles, PATH);

        }
    }
}