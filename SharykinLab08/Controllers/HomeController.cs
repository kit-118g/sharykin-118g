﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SharykinLab08.Models;
using SharykinModel;

namespace SharykinLab08.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        List<PersonFile> files = new()
        {
            new PersonFile()
            {
                EnterTime = new DateTime(2018, 9, 1),
                Facultet = "CIT",
                Group = "118a",
                Person = new Person()
                {
                    Birthday = new DateTime(2001, 02, 14),
                    FirstName = "Nina",
                    LastName = "Medved",
                    MiddleName = "Alekseevna"
                },
                Progress = 100,
                Specializiation = "Computer Engineering",
            },
            new PersonFile()
            {
                EnterTime = new DateTime(2018, 9, 1),
                Facultet = "CIT",
                Group = "118g",
                Person = new Person()
                {
                    Birthday = new DateTime(2000, 9, 28),
                    FirstName = "Konstantin",
                    LastName = "Sharykin",
                    MiddleName = "Valerievich"
                },
                Progress = 53,
                Specializiation = "Computer Engineering",
            }
        };

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(files);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}